/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsadawut.databaseproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author hanam
 */
public class InsertDatabase {
    public static void main(String[] args) {
        Connection conn = null;
        String url ="jdbc:sqlite:dcoffee.db";
        //Connect database
        try {
            conn = DriverManager.getConnection(url);
            System.out.println("Connection to SQLite has been establish.");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return;
        }
        
        //Insert database
        String sql = "INSERT INTO category(cate_id, cate_name) VALUES (?,?)";
            try {
                PreparedStatement stmt  = conn.prepareStatement(sql);
                stmt.setInt(1, 3);
                stmt.setString(2, "candy");
                int status = stmt.executeUpdate();
//                ResultSet key = stmt.getGeneratedKeys();
//                key.next();
//                System.out.println(""+key.getInt(1));
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            
            }
        
        //Close database
        if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    System.out.println(ex.getMessage());
                }
        }
    }
}
