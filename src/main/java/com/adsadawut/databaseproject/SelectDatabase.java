/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsadawut.databaseproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author hanam
 */
public class SelectDatabase {
        public static void main(String[] args) {
        Connection conn = null;
        String url ="jdbc:sqlite:dcoffee.db";
        //Connect database
        try {
            conn = DriverManager.getConnection(url);
            System.out.println("Connection to SQLite has been establish.");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return;
        }
        
        //Select
        String sql = "SELECT * FROM CATEGORY";
            try {
                Statement stmt  = conn.createStatement();
                ResultSet rs =  stmt.executeQuery(sql);
                
                while(rs.next()){
                    System.out.println(rs.getInt("cate_id")+" "
                            +rs.getString("cate_name"));
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            
            }
        
        //Close database
        if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    System.out.println(ex.getMessage());
                }
        }
    }
}
