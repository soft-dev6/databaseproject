/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsadawut.databaseproject.service;

import com.adsadawut.databaseproject.dao.UserDao;
import com.adsadawut.databaseproject.model.User;

/**
 *
 * @author hanam
 */
public class UserService {
    public User logiUser(String name, String password){
        UserDao userDao = new UserDao();
        User user = userDao.getName(name);
        if(user!=null && user.getPassword().equals(password)){
            return user;
        }
        return null;
    }
}
